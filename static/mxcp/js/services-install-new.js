// Equivalent to jquery's "$(document).ready()"
document.addEventListener("DOMContentLoaded", function()
{
    /* Get value for isntallations under path */
    var firstPath= get_installation_path();
    var services            = {};
    var marked_required     = [];
    //var modal      = document.querySelector('#modal-install');
    var checkboxes = document.querySelectorAll('.service__action--grid [type=checkbox]');
    var button     = document.querySelector('.services-button')
    checkboxes.forEach(
        function(checkbox){
          var dependencies = document.querySelectorAll('div[data-service="' + checkbox.dataset.show + '"]');
          if (services[checkbox.getAttribute('name')] = checkbox.checked){
              dependencies.forEach(function(founditem) {
              founditem.classList.add('active'); 
          }); 
          }
            checkbox.addEventListener('click', function(){
                /*
                // Get checked value of the checkbox
                services[checkbox.getAttribute('name')] = checkbox.checked;
                // Is is there any service to be enabled/disabled the modal is 'active'
                if(Object.values(services).indexOf(true) > -1){
                    modal.classList.add('active');
                } else {
                    modal.classList.remove('active');
                }
                */
                var dependencies = document.querySelectorAll('div[data-service="' + checkbox.dataset.show + '"]');
                    dependencies.forEach(function(founditem) {
                    founditem.classList.toggle('active'); 
                });
            });
        }
    );
});

