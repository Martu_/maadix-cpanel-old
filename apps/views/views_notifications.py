# django
from django.utils.translation import ugettext_lazy as _
from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.urls import reverse_lazy, reverse
from django.contrib import messages

# project
from . import utils
from ldap3 import MODIFY_REPLACE
from .forms import EmailForm, EditEmailForm, NotificationForm
from django.conf import settings


class Notifications(FormView):
    """
    Email accounts list view and form to add a new email account
    """

    template_name = 'pages/notifications.html'
    form_class    = NotificationForm
    domains       = {}

    def get_success_url(self):
        """Returns the URL to redirect user after a succesful submit"""
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        context               = super(Notifications, self).get_context_data(**kwargs)
        context['sendermail'] = self.sendermail
        return context

    def get_form(self):
        """Return an instance of the form to be used in this view."""
        emails = utils.get_existing_emails(self.request.ldap)
        self.emails = [ email.mail for email in emails ]
        self.emails.insert(0, 'www-data@%s' % utils.get_server_host() )
        self.sendermail = utils.get_notifications_address(self.request.ldap)
        # Get current configuration for Logs messages
        dn = "ou=logmail_custom,ou=conf,%s" %  settings.LDAP_TREE_CPANEL
        try:
            self.request.ldap.search(
                dn,
                '(ou=logmail_custom)',
                attributes=['status']
                )
            log_mail_status = self.request.ldap.entries[0].status.value
            print("self.log_mail_statusssssssss", log_mail_status)
            self.log_mail_status = utils.ldap_val(log_mail_status)
            print("self.log_mail_status", self.log_mail_status)
        except Exception as e:
            print("There was a problem getting custo logs value the new email")
            print(e)
        kwargs    = self.get_form_kwargs()
        kwargs['initial'] = {
            'log_server' : self.log_mail_status,
            'email'      : self.sendermail
            }
        # Sen puppet status to form. Avoid changing logs receiver if puppet status is not ready
        puppet_status = utils.get_puppet_status(self.request)
        maintenance = puppet_status['puppetstatus']
        return NotificationForm(emails=self.emails, maintenance=maintenance, **kwargs)

    def form_valid(self, form):
        """If the form is valid, save the associated model."""

        try:
            dn = settings.LDAP_TREE_SENDERMAIL
            self.request.ldap.modify(dn, {
                'cn' : [(MODIFY_REPLACE, form['email'].value())]
            })
            # Update the value from log_server. It must be checed if the value has changed
            # If so, lock_cpanel is needed...else not
            # NOTICE: previously sthatus value was alway lowercase
            
            if self.log_mail_status is not form['log_server'].value():
                
                dn = "ou=logmail_custom,ou=conf,%s" %  settings.LDAP_TREE_CPANEL
                self.request.ldap.modify(dn, {
                    'status' : [(MODIFY_REPLACE, form['log_server'].value())]
                })
                try:
                    utils.lock_cpanel(self.request)
                except Exception as e:
                    print("There was a problem locking cpanle")
                    print(e)
                return HttpResponseRedirect( reverse('logout') )

        except Exception as e:
            print("There was a problem setting the new email")
            print(e)

        return super(Notifications, self).form_valid(form)
