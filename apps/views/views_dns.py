# python
import os, re
# django
from django import views
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.urls import reverse_lazy, reverse
from django.utils.encoding import force_bytes
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
# project
from . import utils
from .forms import AddDomainForm, EditDomainForm
from django.conf import settings


class DnsView(views.View):
    """
    Domains list view.
    """

    MESSAGES = {
        'NOT_A_RECORD'   : _("No se ha encontrado ningún registro para el dominio."),
        'A_RECORDS'      : _("El dominio tiene configurado más de un registro de tipo A. "
                             "Esta configuración puede provocar anomalías. A menos que sepas "
                             "exactamente lo que estás haciendo es aconsejable que dejes un "
                             "solo registro."),
        'OK'             : _("La configuración de DNS para el servidor web es correcta"),
        'NOT_OK'         : _("La configuración de los DNS no es correcta para el servidor web. "
                             "Sigue las instrucciones a continuación para corregirla"),
        'NOT_MAILSERVER' : _("El servidor de correo no está activado para este dominio. "
                             "En el caso quisieras activarlo la siguiente tabla te muestra los valores "
                             "DNS correctos."),
        #'CHANGE_IP'      : _("Edita el registro de tipo A cambiando la actual IP %s por %s"),
    }

    def get(self, request):
        # Domain general info
        ip             = utils.get_server_ip()
        host           = utils.get_server_host()
        domain         = request.GET.get('domain') if request.GET.get('domain') else host
        edit_url       = '%s?domain=%s' % (reverse('domains-edit'), domain)
        install_path   = settings.FORCE_SCRIPT_NAME
        # Domain DNS related info
        domain_records = utils.get_dns_records(domain)
        has_A          = 'A' in domain_records
        record         = domain_records['A'][0] if has_A else None
        records_A      = domain_records['A'] if has_A else [None]
        has_MX         = 'MX' in domain_records
        valid_MX       = host +'.'
        records_MX     = [ mx.to_text().split()[1] for mx in domain_records['MX'] ] if has_MX else [None]
        has_TXT        = 'TXT' in domain_records
        is_mailman_domain = False
        try:
            record_SPF    = [ txt for txt in domain_records['TXT'] if 'v=spf' in txt.to_text() ][0] if has_TXT else [None]
        except Exception as e:
            record_SPF    =  None

        valid_SPF      = "\"v=spf1 mx ip4:%s a:%s ~all\"" % ( ip, host)
        record_DKIM   = utils.get_dkim(domain)
        has_dkim       = None
        dkim_domain    = "default._domainkey.%s" % domain
        dkim_matches    = False
        mx_matches      = False
        msg            = {}

        mailman_domains = utils.get_mailman_domain_names()

        try:
            # look for the domain in LDAP
            request.ldap.search(
                'vd=%s,%s' % (domain,settings.LDAP_TREE_HOSTING),
                '(objectClass=VirtualDomain)',
                attributes=['accountActive']
            )
        except Exception as e:
            # if querying a wrong domain redirect to domains overview
            print("LOOKING FOMR DOMAIN", domain)
            if not domain in mailman_domains and not domain == host:
                messages.error(self.request, _('No tienes ese dominio creado'))
            else:
                is_mailman_domain = True
                print("IS MAILMAN DOMAIN ", is_mailman_domain)

        # Check A records
        if not has_A:
            msg['A'] = self.MESSAGES['NOT_A_RECORD']
        else:
            A_records_number = len(domain_records['A'])
            if  A_records_number == 1 and '%s' % record == '%s' % ip:
                msg['A'] = self.MESSAGES['OK']
            elif A_records_number > 1 and ip in records_A:
                msg['A'] = self.MESSAGES['A_RECORDS']
            else:
                msg['A'] = self.MESSAGES['NOT_OK']

        # Check MX records
        if records_MX :
            print('records_MX', records_MX)
            if len(records_MX ) < 2 and records_MX[0] == valid_MX:
                mx_matches = True
            #valid_MX   = host 
        if not is_mailman_domain and not domain==host:
            mail_active = utils.ldap_val(request.ldap.entries[0].accountActive.value)
        """
        if not mail_active:
            msg['MX'] = self.MESSAGES['NOT_MAILSERVER']
        """
        # Check DKIM record
        try:
            request.ldap.search(
                "ou=%s,%s" % (domain,settings.LDAP_TREE_DKIM ),
                "(objectClass=organizationalUnit)"
            )
            has_dkim = True
        except Exception as e:
            print("There was a problem retrieving DKIM entry in LDAP: ")
            print( str(e) )
            msg['DKIM'] = _('La clave DKIM no está activada para este dominio')

        #has_dkim = len(request.ldap.entries) > 0
        if has_dkim or is_mailman_domain:
            # Get Dkim dns record
            record_DKIM    = utils.get_dkim(domain)
            # Get dkim vañue from server file
            required_dkim = utils.get_local_dkim_key(domain)
            # Compare values in string format
            if(record_DKIM and required_dkim):
                dkim_matches = utils.compare_dkim_values(record_DKIM,required_dkim)
            if required_dkim:
                msg['DKIM'] = required_dkim
                print("REQUIRED DKIM " , required_dkim)
            else:
                msg['DKIM'] = _('Todavía no se ha generado ninguna clave dkim para el dominio %s. Este proceso puede tardar unos minutos.' % domain)
        else:
            editurl = reverse('domains-edit') + '?domain=' + domain
            msg['DKIM'] = _('La clave DKIM no está activada para este dominio.')

        return render(request, 'pages/dns.html', locals())
