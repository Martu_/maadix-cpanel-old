# python
import urllib3, os, json, pwd, certifi

# django
from django.utils.translation import ugettext_lazy as _, get_language
from django import views
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.views.generic.edit import FormView
# contrib
from ldap3 import MODIFY_REPLACE

# project
from .utils import get_release_info, connect_ldap, get_puppet_status, p, lock_cpanel, get_server_ip, check_domain_avalability,split_dependencies
from django.conf import settings
from . import debug
from .forms import InstallAppForm, UpdateAppForm

class Services(FormView):
    template_name = 'pages/services.html'
    form_class    = UpdateAppForm
    ldap          = {}
    ldap_attributes = ['status']
    dep_status    = {}

    def get_success_url(self):
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):

        context = super(Services, self).get_context_data(**kwargs)
        context['not_disabled'] = ['mail' ,'nodejs']

        #context['status'] = self.puppet_status
        #context['release'] = self.release_info['release']
        #context['maintenance'] = self.maintenance
        #context['available'] = self.services_available
        #context['all_installed'] = self.allinstalled
        #context['no_updates'] = self.no_updates
        context['show_modal'] = 'form' in kwargs
        #context['groups'] = 'form' in kwargs	
        context['enabled'] = self.enabled_services
        context['disabled'] = self.disabled_services
        context['all_services'] = self.all_services
        #context['dep_groups'] = 'form' in kwargs
        #print('CONTEXT ALL SERVCES', context['all_services'])
	
        #context['show_modal']=False
        return context

    def get(self, request):
        self.release_info  = get_release_info(request)
        self.puppet_status = get_puppet_status(request)
        # list of dict with all info from API about enabled or disabled services
        all_services = []
        #self.allinstalled = False
        #self.no_updates = False

        try:
            lang = '_' + get_language()
        except:
            lang = '_es'

        try:
            """
            request.ldap.search(
                settings.LDAP_TREE_BASE,
		settings.LDAP_FILTERS_INSTALLED_SERVICES_ALL,
		attributes=['ou']
		)
            self.enabled_services = [ service.ou.value for service in request.ldap.entries ]
            """
            self.enabled_services=self.request.enabled_services
	    # Exclude installed services from available services
	    #self.services_available = [ service for service in all_services if service['id'] not in installed_services ]
            request.ldap.search(
		settings.LDAP_TREE_BASE,
		settings.LDAP_FILTERS_INSTALLED_DISABLED_SERVICES,
		attributes=['ou']
	    )
            self.disabled_services = [ service.ou.value for service in request.ldap.entries ]

            if 'groups' in self.release_info:
		# Get all services, enabled ir disabled 
                alllang_services = self.release_info.get('groups')
		# Filter services by language
                for serv in alllang_services:
                    if serv['id'].endswith(lang):
                        serv.update({'id': serv['id'][:-3]})
                        print('NEW SERV ID ', serv['id'])
                        if serv['id'] in self.disabled_services or serv['id'] in self.enabled_services:
                            all_services.append(serv)
        except Exception as e:
            # Maybe mark a difference between no available aplicaction due to 'All of them have been installed' and a 
            # problem retreiving available application from ldap or whatever error
            # Now both cases are the same and sets context['no_updates']  = True
            p("ServicesAvailable view", "✕ There was a problem retrieving enabled services", e)
        self.all_services=all_services
        
        return super(Services, self).get(request)
        #return self.render_to_response(self.get_context_data())

    def get_form(self):
        return self.form_class(enabled_services=self.enabled_services,disabled_services=self.disabled_services,all_services=self.all_services, **self.get_form_kwargs())

    def post(self, request, *args, **kwargs):
        data = self.get(request)
        form = self.get_form()
        #an empty list to store all gorups to be installed
        disable_services = []
        enable_services = []
        enable_group_deps = []
        disable_group_deps = []
        deps_needed = []
        context = self.get_context_data(**kwargs)
        if form.is_valid() and request.method=='POST':
            for service in self.all_services:
                app_name = service['id']
                field_name = "disable-%s" % app_name 
                # First get checked application to be disabled
                if app_name in self.enabled_services and form[field_name].value():
                    # Add app name to apps to be disabled 
                    disable_services.append(app_name)    
                    
                    # Check dependendies:
                    if service.get('dependencies'):
                        # Get group dependecies dependencies.
                        # It is a dymanic generate form, so we don't know
                        # Whic fields are included within.
                        # We need to get this info from the Dependencies API string    
                        app_dep = split_dependencies(service['dependencies'])
                        for dep in app_dep:
                            # record apps dependencies, which have length 1 
                            if len(dep) == 1:
                                field_name = "dependency-%s" % app_name 
                                if form[field_name].value() and not form[field_name].value() in context['not_disabled']:
                                    # If app_name has a dependenciy group check if it can be
                                    # disabled or if is in use by another app
                                    disable_group_deps.append(form[field_name].value())
                field_name = "enable-%s" % app_name
                # First get checked application to be disabled
                if app_name in self.disabled_services and form[field_name].value():
                    # Add app name to apps to be disabled 
                    enable_services.append(app_name)

                    # Check dependendies:
                    if service.get('dependencies'):
                        # Get group dependecies dependencies.
                        # It is a dymanic generate form, so we don't know
                        # Whic fields are included within.
                        # We need to get this info from the Dependencies API string    
                        app_dep = split_dependencies(service['dependencies'])
                        for dep in app_dep:
                            # record apps dependencies, which have length 1 
                            if len(dep) == 1:
                                field_name = "dependency-%s" % app_name
                                dep_group = form[field_name].value()
                                if dep_group and (dep_group not in enable_group_deps or dep_group not in enable_services):
                                    # If app_name has a dependenciy group check if it can be
                                    # disabled or if is in use by another app
                                    enable_group_deps.append(form[field_name].value())
            # Check if candidates dependencies can be disabled or if are used from other apps
            # Need to check this after post, after groups are checked and actions are defined
            for all_serv in self.all_services:
                if all_serv.get('dependencies'):
                    app_dep = split_dependencies(all_serv['dependencies'])
                    for dep in app_dep:
                        if len(dep) == 1 and all_serv['id'] in self.enabled_services:
                            deps_needed.append(dep[0])
            #print('DEPS NEEDED', deps_needed)
            print('DEPS NEEDED', deps_needed)
            """
            for dep in disable_group_deps:
                if dep in deps_needed or dep in context['not_disabled']:
                    disable_group_deps.remove(dep)
            """
            """
            for service in disable_services:
                if service in deps_needed and service not in disable_group_deps:
                    messages.error(self.request, _('La aplicación %s no se puede desactivar por que está en uso por otras apicaciones' % service))
                    return self.render_to_response(context)
            """
            # Used in modal confirmation to list apps to be enabled/disabled
            # Remove items from disable_group_deps if are already in disable_services
            #TODO: If a service with dependency A is to be disabled and at the same tine
            #another service with same dependency is to be 
            # enabled, do not disable dependency A.
            # For ex. if a dependency is marked to collect all emabled and disabled dependecies.
            # For any match, remove it from disable an leave it in enable 
            print('DISABLE DEPENDENCY BEFOREEEEEEEEEEEE', disable_group_deps)
            print('SELF ENABLED SERVICES', self.enabled_services)
            print('ENABLE GROUPS', enable_services)
            print('ENABLE GROUP DEPS' , enable_group_deps)
            print('DISABLE GROUPS', disable_services)
            print('DISABLE DEPENDENCY BEFOREEEEEEEEEEEE', disable_group_deps)
            clean_disable_dep  = [item for item in disable_group_deps if item not in disable_services and item not in enable_group_deps]
            clean_enable_dep  = [item for item in enable_group_deps if item not in enable_services and item not in self.enabled_services]
            context['disable_groups'] = disable_services
            context['disable_dep_groups'] = clean_disable_dep
            context['enable_groups'] = enable_services
            context['enable_dep_groups'] = clean_enable_dep
            
            print('CLEAN DISABLE DEPENDENCY', clean_disable_dep) 
            print('CLEAN ENABLE GROUPS DEPS', clean_enable_dep) 
            for service in disable_services:
                if service in enable_group_deps or (service in deps_needed and service not in disable_group_deps):
                    messages.error(self.request, _('La aplicación %s no se puede desactivar por que es necesaria para  otras apicaciones seleccionadas o activadas' % service))
                    return self.render_to_response(context)
            context['show_modal'] = True
            # Check if app in dep_groups is in use
            if request.method=='POST' and 'writeldap' in request.POST:
                services_enable=enable_services + clean_enable_dep
                services_disable = disable_services

                #self.services_enable=enable_services
                #self.services_disable = disable_services
                try:
                    #services = json.loads(request.body.decode('utf-8'))
                    print('SEEEEEEEEEEEEEE', services_disable)
                    # update services to be disabled
                    if services_disable:
                        for service in services_disable:
                            dn = 'ou=%s,%s' % (service, settings.LDAP_TREE_SERVICES)
                            print('DN IS? ' , dn)
                            request.ldap.modify(dn, {
                                'type'   : [(MODIFY_REPLACE, 'installed')],
                                'status' : [(MODIFY_REPLACE, 'disabled')],
                            })
                    print('SEEEEEEEEEEEEEE', services_enable)
                    # update services to be enabled
                    if services_enable:
                        for service in services_enable:
                            dn = 'ou=%s,%s' % (service, settings.LDAP_TREE_SERVICES)
                            print('DN IS? ' , dn)
                            request.ldap.modify(dn, {
                                'type'   : [(MODIFY_REPLACE, 'available')],
                                'status' : [(MODIFY_REPLACE, 'enabled')],
                            })
                    # lock cpanel
                    lock_cpanel(request)

                    #messages.success(self.request, _('Aplicaciones modificadas con éxito'))
                    return HttpResponseRedirect( reverse('logout') )

                except Exception as e:
                    messages.error(self.request, _('Hubo algún problema modificando el estado de tus aplicaciones. '
                                                   'Contacta con los administrador-s si el problema persiste'))
                    print(e)
                    context['show_modal'] = False
                    #return HttpResponse("Something failed trying to update services in LDAP", status=500)
                    return self.render_to_response(context)

        else:
            context['show_modal'] = False 
        #return self.form_invalid(form, **kwargs)
        return self.render_to_response(context)

class AvailableAppsview(FormView):
    template_name = 'pages/appsinstall.html'
    form_class    = InstallAppForm
    ldap          = {}
    ldap_attributes = ['status']
    dep_status    = {}
    inputdeps     = []
    #maintenance   = False

    def get_success_url(self):
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):

        context = super(AvailableAppsview, self).get_context_data(**kwargs)
        context['status'] = self.puppet_status
        context['release'] = self.release_info['release']
        #context['maintenance'] = self.maintenance
        context['available'] = self.services_available
        context['all_installed'] = self.allinstalled
        context['no_updates'] = self.no_updates
        context['show_modal'] = 'form' in kwargs
        context['groups'] = 'form' in kwargs	
        context['dep_groups'] = 'form' in kwargs

	
        #context['show_modal']=False
        return context

    def get(self, request):
        self.release_info  = get_release_info(request)
        self.puppet_status = get_puppet_status(request)
        all_services = []
        self.allinstalled = False
        self.no_updates = False
        try:
            lang = '_' + get_language()
        except:
            lang = '_es'

        try:
            if 'groups' in self.release_info:
                # Get all available services, installed or not
                alllang_services = self.release_info.get('groups')
                # Filter services by language
                for serv in alllang_services:
                    if serv['id'].endswith(lang):
                        serv.update({'id': serv['id'][:-3]})
                        all_services.append(serv)

                request.ldap.search(
                    settings.LDAP_TREE_BASE,
                    settings.LDAP_FILTERS_INSTALLED_SERVICES_ALL,
                    attributes=['ou']
                    )
                installed_services = [ service.ou.value for service in request.ldap.entries ]
                # Exclude installed services from available services
                self.services_available = [ service for service in all_services if service['id'] not in installed_services ]
                if not self.services_available:
                    self.allinstalled = True
        except Exception as e:
            # Maybe mark a difference between no available aplicaction due to 'All of them have been installed' and a 
            # problem retreiving available application from ldap or whatever error
            # Now both cases are the same and sets context['no_updates']  = True
            self.no_updates = True
            p("ServicesAvailable view", "✕ There was a problem retrieving enabled services", e)
            if self.puppet_status['puppetstatus'] == 'error' or self.puppet_status['puppetstatus'] == 'pending':
                self.maintenance= True
        return super(AvailableAppsview, self).get(request)
        #return self.render_to_response(self.get_context_data())

    def get_form(self):
        return InstallAppForm(request=self.request,services_available=self.services_available, **self.get_form_kwargs())

    def post(self, request, *args, **kwargs):
        data = self.get(request)
        form = self.get_form()
        #an empty list to store all gorups to be installed
        services = []
        group_deps = []
        #an epmty dict to store user input dependencie
        dependencies= {}
        dependencies["app"]=[]
        context = self.get_context_data(**kwargs)
        if form.is_valid():
            for service in self.services_available:
                app_name = service['id']
                
                # First get checked application
                if form[app_name].value():
                    # Add app name to apps to be isntalled in ldap
                    services.append(app_name)    
                    
                    # Check dependendies:
                    if service.get('dependencies'):
                        # Get all user inputs and default dependencies.
                        # It is a dymanic generate form, so we don't know
                        # Whic fields are included within.
                        # We need to get this info from the Dependencies API string    
                        app_dep = split_dependencies(service['dependencies'])
                        for dep in app_dep:
                            # record apps dependencies, which have length 1 
                            if len(dep) == 1:
                                field_name = "dependency-%s" % app_name 
                                if form[field_name].value():
                                    group_deps.append(form[field_name].value())
                            # Record user inputs, which have length > 2

                            elif len(dep) > 2: 
                                field_name = "input-required-%s-%s" % (app_name, dep[0])
                                if form[field_name].value():
                                    args = {
                                            'name'   : dep[0],
                                            'service': app_name,
                                            'value'  : form[field_name].value().strip(),
                                            }

                                    dependencies["app"].append(args)

            context['show_modal'] = True
            context['groups'] = services
            context['dep_groups'] = group_deps
            if request.method=='POST' and 'writeldap' in request.POST:
                services=services+group_deps
                for service in services:
                    if settings.DEBUG_INSTALL:
                        print("Installing service ", service)
                    else:
                        try:
                            dn = "ou=%s,%s" % (service, settings.LDAP_TREE_SERVICES)
                            request.ldap.search(
                                dn,
                                "(objectclass=*)",
                                attributes = [ 'dn']
                            )
                            if request.ldap.entries:
                                request.ldap.modify(dn, {'status' : [(MODIFY_REPLACE, 'install')]})
                            else:
                                p("ServicesInstall view", "✕ service Nt found", e)
                        except Exception as e:
                            try:
                                self.request.ldap.add(dn, [
                                    'organizationalUnit',
                                    'metaInfo',
                                ], {
                                    'ou' : service,
                                    'status' : 'install',
                                    'type'   : 'available',
                                })
                            except Exception as e:
                                p("ServicesInstall view", "✕ could not add service ", e)
                # Add dependencie
                # This are user input dependencies, that are inserted as subtree of the app they belong
                for dependency in dependencies['app']:
                    create_dep = False
                    if settings.DEBUG_INSTALL:
                        print("Installing dependency ", dependency)
                    else:
                        try:
                            dn = 'ou=%s,ou=%s,%s' % (dependency["name"], dependency["service"], settings.LDAP_TREE_SERVICES);
                            request.ldap.search(
                                dn,
                                "(objectclass=*)",
                                attributes = [ 'dn']
                            )
                            if request.ldap.entries:
                                request.ldap.modify(dn, {'status' : [(MODIFY_REPLACE, dependency["value"])]})
                            else:
                                p("ServicesInstall view", "✕ entry for service deps does not exists", e)
                        except Exception as e:
                            try:
                                self.request.ldap.add(dn, [
                                    'organizationalUnit',
                                    'metaInfo',
                                ], {
                                    'ou'     : dependency["name"],
                                    'status' : dependency["value"],
                                })
                            except Exception as e:
                                ("ServicesInstall view", "✕ Could not create dependency tree", e)
                lock_cpanel(request)
                # messages.success(self.request, _('Aplicaciones instaladas con éxito'))
                return HttpResponseRedirect( reverse('logout'))

        else:
            context['show_modal'] = False 
        #return self.form_invalid(form, **kwargs)
        return self.render_to_response(context)
