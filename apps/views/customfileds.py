import copy
from django import forms
from django.forms.fields import Field
from . import widgets

class HeaderField(Field):
    widget = widgets.HeaderWidget
    validators = []
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def clean(self, value=''):
        """
        Validate the given value and return its "cleaned" value as an
        appropriate Python object. Raise ValidationError for any errors.
        """
        return value
