#python
import json
# django
from django import views
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
from django.http import  HttpResponse, JsonResponse
from django.conf import settings
# project
from . import utils

class CheckCpanelStatus(views.View):
    """ View to delete an entry from LDAP """
    #@method_decorator(csrf_exempt)
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        """ Need to allow post from others domains and without token, 
            for cases of fqdn change """
        response = super(CheckCpanelStatus, self).dispatch(*args, **kwargs)
        response["Access-Control-Allow-Headers"] = "X-CSRFToken"
        response["Access-Control-Allow-Origin"] = "*" 
        response["Access-Control-Allow-Methods"] = "POST, OPTIONS"
        return response

    def post(self, request):

        try:
            ldap = utils.anonymous_connect_ldap()
            status = utils.get_cpanel_status(ldap)
        except Exception as e:
            if settings.DEBUG:
                print(e)
            # If there is some error accesing LDAP the cpanel is lcoked 
            status = 'locked'
        return HttpResponse (status)
"""
class CheckCpanelApacheStatus(views.View):

    def post(self, request):
        
        try:
            ldap = utils.anonymous_connect_ldap()
            status = utils.get_cpanel_status(ldap)
            #status = 'locked'
            apache = utils.find_procs_by_name("apache2")
        except Exception as e:
            if settings.DEBUG:
                print(e)
            # If there is some error accesing LDAP the cpanel is lcoked 
            status = 'locked'
            apache = True
        results = '{"cpanel" : "%s", "apache" : "%s"}' % (status,apache)

        return HttpResponse(results)
"""
