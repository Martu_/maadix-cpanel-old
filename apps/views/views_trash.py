# python
import json

# django
from django.utils.translation import ugettext_lazy as _, get_language
from django import views
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.generic.edit import FormView
# contrib
from ldap3 import MODIFY_REPLACE

# project
from . import utils 
from django.conf import settings
from . import debug
from .forms import TrashForm 

class Trash(FormView):
    template_name = 'pages/trash.html'
    form_class    = TrashForm
    ldap          = {}

    def get_success_url(self):
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        context = super(Trash, self).get_context_data(**kwargs)
        context['show_modal'] = 'form' in kwargs
        context['trashContent'] = self.trashContent
        if (self.trash_status == 'error'):
            context['modal_text_body'] =_("Si continúas se reanudará la última operación de borrado que generó un error y se eliminarán de forma permanente las carpetas previamente seleccionadas. Esta acción no se puede deshacer.")
        else:
            context['modal_text_body'] =_("Si continúas se borrarán de forma permanente las carpetas seleccionadas. Esta acción no se puede deshacer.")
        context['modal_text_confirm'] =_("¿Confirmas que quieres continuar?")
        context['trash_status'] = self.trash_status
        return context

    def get(self, request):
        trashContent={}
        subtrees = ['domains', 'users']
        # Get trash module status in ldap. If status is serror  show a button to rcover previous failed operation
        self.trash_status = utils.get_cpanel_local_status(self.request.ldap, 'trash')     
        for item in subtrees:
            try:
                self.request.ldap.search('ou=%s,%s' % (item, settings.LDAP_TREE_TRASH),
		    '(|(&(objectClass=metaInfo)(status=intrash))(&(objectClass=metaInfo)(status=totrash)))',
		    attributes=['type', 'otherPath','cn','description','status'])
                trashContent[item]=self.request.ldap.entries 
            except Exception as e:
                messages.error(self.request, _('Se ha producido un error leyendo la Papelera'))
        self.trashContent= trashContent
        return super(Trash, self).get(request)        

    def get_form(self):
        return self.form_class(request=self.request,trashContent=self.trashContent, **self.get_form_kwargs())

    def post(self, request, *args, **kwargs):
        data = self.get(request)
        form = self.get_form()
        context = self.get_context_data(**kwargs)
        # Get all checked iputs
        if form.is_valid() and request.method=='POST':
            context['show_modal'] = True

        if request.method=='POST' and 'writeldap' in request.POST:
            for field in form:
                if field.value(): 
                    base    = settings.LDAP_TREE_TRASH
                    try: 
                        self.request.ldap.search(
                            base,
                            '(&(objectClass=applicationProcess)(cn=%s))'%field.name,
                            attributes    = [ 'cn']
                        )
                        dn = self.request.ldap.entries[0].entry_dn
                        self.request.ldap.modify(dn, { 'status' : [(MODIFY_REPLACE, 'purge')] })
                    except Exception as e:
                        print('error: ' ,e)
                        messages.error(self.request, _('Se ha producido un error'))

            context['show_modal'] = False
            messages.success(self.request, _('Cambios aplicados con éxito'))
            utils.lock_cpanel_local(self.request.ldap, 'trash')
            return HttpResponseRedirect(self.request.get_full_path())

        return self.render_to_response(context)
